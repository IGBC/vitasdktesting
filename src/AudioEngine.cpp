#include "AudioEngine.h"

#include <algorithm>
#include <cstring>
#include <stdexcept>
#include <string>

AudioEngine::AudioEngine(int freq): maxSongID(0), maxInstanceID(0) {
    targetSpec.freq = freq;
    targetSpec.format = AUDIO_S16;
    targetSpec.channels = 2;
    targetSpec.samples = 1024;
    targetSpec.callback = staticAudioCallback;
    targetSpec.userdata = static_cast<void*>(this);

    device = SDL_OpenAudioDevice(NULL, 0, &targetSpec, &actualSpec, 0);
    if (device == 0) {
        std::string err_msg("failed to open audio device: ");
        err_msg.append(SDL_GetError());
        throw std::runtime_error(std::move(err_msg));
    }
    SDL_PauseAudioDevice(device, 0);
}

AudioEngine::~AudioEngine() {
    SDL_CloseAudioDevice(device);
}

AudioEngine::AudioIndex AudioEngine::loadAudioFile(const char* name) {
    uint8_t* buffer;
    uint32_t len;
    SDL_AudioSpec* resultSpec = SDL_LoadWAV(name, &actualSpec, &buffer, &len);
    if (resultSpec == NULL) {
        printf("Failed to load %s: %s\n", name, SDL_GetError());
        return -1;
    }

    if ((resultSpec->freq != actualSpec.freq) || (resultSpec->format != actualSpec.format)) {
        SDL_FreeWAV(buffer);
        return -1;
    }

    int16_t* newBuffer = new int16_t[len/2];
    if (newBuffer != NULL) {
        memcpy(newBuffer, buffer, len);
        AudioIndex songID = maxSongID++;
        songs.emplace_back(std::make_shared<AudioBuffer>(newBuffer, len/2, resultSpec->channels, songID));
        SDL_FreeWAV(buffer);
        return songID;
    } else {
        SDL_FreeWAV(buffer);
        return -1;
    }
}

AudioEngine::PlaybackIndex AudioEngine::playAudioFile(AudioIndex buffer, bool loop) {
    auto is_id = [=](std::shared_ptr<AudioBuffer> a){ return a->id == buffer;};
    auto result = std::find_if(songs.begin(), songs.end(), is_id);
    if ( result != songs.end() ) {
        auto id = maxInstanceID++;
        instances.emplace_back(*result, loop, id);
        return id;
    } else {
        return -1;
    }
}

void AudioEngine::audioCallback(int16_t* stream, int len) {
    for (int i = 0; i < len; i+=2) { // two channels
        int16_t left = 0;
        int16_t right = 0;
        for ( auto instance = instances.begin(); instance != instances.end();) {
            auto sample = instance->buffer;
            if (sample->channels == 2) {
                left += sample->ptr[instance->played++];
                right += sample->ptr[instance->played++];
            } else {
                int16_t s = sample->ptr[instance->played++];
                left += s;
                right += s;
            }
            if (instance->played >= sample->len) {
                if (instance->loop) {
                    instance->played = 0;
                    ++instance;
                } else {
                    instance = instances.erase(instance);
                }
            } else {
                ++instance;
            }
        } 
        stream[i] = left;
        stream[i+1] = right;
    }
}