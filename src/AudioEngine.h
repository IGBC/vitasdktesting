#pragma once
#include <SDL2/SDL_audio.h>
#include <memory>
#include <vector>

class AudioEngine {
public:
    using AudioIndex = size_t;
    using PlaybackIndex = size_t;
private:
    struct AudioBuffer {
        int16_t* ptr;
        uint32_t len;
        uint8_t channels;
        AudioIndex id;

        AudioBuffer(int16_t* ptr, uint32_t len, uint8_t channels, AudioIndex id) :
            ptr(ptr), len(len), channels(channels), id(id) {};
        AudioBuffer(const AudioBuffer&) = delete; //not shallow copiable;
        AudioBuffer(AudioBuffer&&) = default;
        AudioBuffer& operator=(const AudioBuffer&) = delete;
        AudioBuffer& operator=(AudioBuffer&&) = delete;

        ~AudioBuffer() {
            delete[] ptr;
        }
    };

    struct AudioBufferPtr {
        std::shared_ptr<AudioBuffer> buffer;
        uint32_t played;
        bool loop;
        PlaybackIndex id;

        AudioBufferPtr(std::shared_ptr<AudioBuffer> buffer, bool loop, PlaybackIndex id) : buffer(buffer), played(0), loop(loop), id(id) {}; 
        
    };

    std::vector<std::shared_ptr<AudioBuffer>> songs;
    std::vector<AudioBufferPtr> instances;

    SDL_AudioSpec targetSpec, actualSpec;
    SDL_AudioDeviceID device;

    AudioIndex maxSongID;
    PlaybackIndex maxInstanceID;

    static void staticAudioCallback(void *args, uint8_t* stream, int len) {
        AudioEngine* obj = static_cast<AudioEngine*>(args);
        int16_t* stream16 = reinterpret_cast<int16_t*>(stream);
        int len16 = len / 2; // two bytes
        obj->audioCallback(stream16, len16);
    }

    void audioCallback(int16_t* stream, int len);

public:
        

    AudioEngine(const int freq);
    AudioEngine(AudioEngine&) = delete;
    ~AudioEngine();

    AudioIndex loadAudioFile(const char* name);
    PlaybackIndex playAudioFile(AudioIndex buffer, bool loop);
};