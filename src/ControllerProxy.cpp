#include "ControllerProxy.h"

#include <SDL2/SDL.h>

ControllerProxy::ControllerProxy(int id) : stick_id(id),
    cross(0), circle(0), triangle(0), square(0), start(0), select(0),l1(0), r1(0),
    dpad_up(0), dpad_down(0), dpad_left(0), dpad_right(0),
    stick_left_x(0), stick_right_x(0), stick_left_y(0), stick_right_y(0)
{
    SDL_GameController *joystick = SDL_GameControllerOpen(id);
    stick_ptr = reinterpret_cast<void*>(joystick);
    printf("Opened Stick %i -> %s\n", id, SDL_JoystickNameForIndex(id));
}

ControllerProxy::~ControllerProxy() {
    SDL_GameController *joystick = reinterpret_cast<SDL_GameController*>(stick_ptr);
    SDL_GameControllerClose(joystick);
}



bool ControllerProxy::read_button(Button button) {
    switch (button) {
    case Cross:
        return cross;
    case Circle:
        return circle;
    case Triangle:
        return triangle;
    case Square:
        return square;
    case Start:
        return start;
    case Select:
        return Select;
    case L1:
        return l1;
    case R1:
        return r1;    
    default:
        return 0;    
    }
}

ControllerProxy::DpadDirection ControllerProxy::read_dpad() {
    int x = 0;
    int y = 0;
    if (dpad_up)   y -= 1;
    if (dpad_down) y += 1;
    if (dpad_left)  x -= 1;
    if (dpad_right) x += 1;
    switch (y) {
    case -1:
        switch (x) {
            case -1: return LeftUp;
            case  0: return Up;
            case  1: return RightUp; 
        }
    case 0:
        switch (x) {
            case -1: return Left;
            case  0: return None;
            case  1: return Right; 
        }
    case 1:
        switch (x) {
            case -1: return LeftDown;
            case  0: return Down;
            case  1: return RightDown; 
        }
    default:
        return None;
    }
}

void ControllerProxy::set_button(uint8_t button, uint8_t state) {
    switch (button) {
        case SDL_CONTROLLER_BUTTON_A:
            cross = (state == SDL_PRESSED) ? 1 : 0;
            break;
        case SDL_CONTROLLER_BUTTON_B:
            circle = (state == SDL_PRESSED) ? 1 : 0;
            break;
        case SDL_CONTROLLER_BUTTON_X:
            square = (state == SDL_PRESSED) ? 1 : 0;
            break;
        case SDL_CONTROLLER_BUTTON_Y:
            triangle = (state == SDL_PRESSED) ? 1 : 0;
            break;
        case SDL_CONTROLLER_BUTTON_DPAD_UP:
            dpad_up = (state == SDL_PRESSED) ? 1 : 0;
            break;
        case SDL_CONTROLLER_BUTTON_DPAD_DOWN:
            dpad_down = (state == SDL_PRESSED) ? 1 : 0;
            break;
        case SDL_CONTROLLER_BUTTON_DPAD_LEFT:
            dpad_left = (state == SDL_PRESSED) ? 1 : 0;
            break;
        case SDL_CONTROLLER_BUTTON_DPAD_RIGHT:
            dpad_right = (state == SDL_PRESSED) ? 1 : 0;
            break;
        case SDL_CONTROLLER_BUTTON_BACK:
            select = (state == SDL_PRESSED) ? 1 : 0;
            break;
        case SDL_CONTROLLER_BUTTON_START:
            start = (state == SDL_PRESSED) ? 1 : 0;
            break;
        case SDL_CONTROLLER_BUTTON_LEFTSHOULDER:
            l1 = (state == SDL_PRESSED) ? 1 : 0;
            break;
        case SDL_CONTROLLER_BUTTON_RIGHTSHOULDER:
            r1 = (state == SDL_PRESSED) ? 1 : 0;
            break;
        default:
            printf("Unknown button %i pressed/released\n", button);
    }
}

bool ControllerProxy::update() {
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
        switch (event.type) {
            case SDL_QUIT:
                printf("Quit Event\n");
                return 0;
                break;
            case SDL_CONTROLLERBUTTONDOWN:
            case SDL_CONTROLLERBUTTONUP:
                printf("Button Event %i : %i\n", event.cbutton.button, event.cbutton.state);
                if (event.cbutton.which == stick_id) {
                    set_button(event.cbutton.button, event.cbutton.state);
                }
                break;
            case SDL_CONTROLLERAXISMOTION:
                
            default:
                break;
        }
    }
    return 1;
}