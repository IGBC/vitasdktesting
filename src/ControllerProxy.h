#pragma once

#include <cstdint>
#include <utility>

class ControllerProxy {
public:
    enum Button { Cross, Circle, Triangle, Square, Start, Select, L1, R1 };
    enum DpadDirection { None = 0, LeftDown, Left, LeftUp, Up, RightUp, Right, RightDown, Down };
    enum Stick { LeftStick, RightStick };

    bool read_button(Button);
    DpadDirection read_dpad();
    std::pair<float,float> read_stick(Stick);

    ControllerProxy(int id);
    ControllerProxy(const ControllerProxy&) = delete;
    ControllerProxy& operator=(const ControllerProxy&) = delete;
    ~ControllerProxy();

    bool update();

private:

    void *stick_ptr;
    int stick_id;

    DpadDirection dpad;
    bool cross, circle, triangle, square, start, select, l1, r1;
    bool dpad_up, dpad_down, dpad_left, dpad_right;
    float stick_left_x, stick_left_y, stick_right_x, stick_right_y;
    void set_button(uint8_t button, uint8_t state);
};

