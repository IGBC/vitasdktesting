#include "game.h"
//#include <psp2/ctrl.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <string>

Game::Game(SDL_Renderer* ctx):x(SCREEN_HEIGHT), y(SCREEN_WIDTH/2), dx(0), dy(0), audio(44100), controls(0) {
    std::string msg("You have 30 minutes to move your cube");
    auto ahandle = audio.loadAudioFile("res/moveyourcube.wav");
    if (ahandle == -1) {
        msg = SDL_GetError();
    } else {
        audio.playAudioFile(ahandle, true);
    }
    
    TTF_Init();
    TTF_Font* font = TTF_OpenFont("res/Ubuntu-R.ttf", 36);
    SDL_Surface* stext = TTF_RenderText_Solid(font, msg.c_str(), {128,128,128});
    text = SDL_CreateTextureFromSurface(ctx, stext);
    SDL_FreeSurface(stext);
    TTF_CloseFont(font);

    SDL_Surface* image_surface = IMG_Load("res/no-parking.png");
    sign = SDL_CreateTextureFromSurface(ctx, image_surface);
    SDL_FreeSurface(image_surface);

    
};

bool Game::update(float delta) {
    // scan inputs
    if (!controls.update())
        return 0;

    // Move your cube
    if (y == 0) {
        if (controls.read_dpad() == ControllerProxy::Left)
            dx-=2;
        if (controls.read_dpad() == ControllerProxy::Right)
            dx+=2;
    }

    if (controls.read_button(ControllerProxy::Cross))
        dy = 10;

    // physics
    dy --;
    y += dy;
    x += dx;
    if (y <= 0){
        y = 0;
        dy = 0;
        if (dx < 0)
            dx++;
        if (dx > 0)
            dx--;
    }
        
    return 1; //return !(ctrl.buttons & SCE_CTRL_START);
}

void Game::draw(SDL_Renderer* ctx) {
    const SDL_Rect fillRect = { x, SCREEN_HEIGHT - y, 64, -64 };
    int w, h;
    SDL_QueryTexture(text, NULL, NULL, &w, &h);
    const SDL_Rect textRect = { (SCREEN_WIDTH / 2) - (w / 2), (SCREEN_HEIGHT / 2) - (h / 2), w, h};
    constexpr SDL_Rect signRect = { ((SCREEN_WIDTH * 2) / 3) - (64), SCREEN_HEIGHT - 128, 64, 128};
    
    SDL_SetRenderDrawColor( ctx, 68, 54, 143,255);
    SDL_RenderClear(ctx);
    SDL_SetRenderDrawColor( ctx, 228, 91, 252 ,255);
    SDL_RenderCopy(ctx, text, NULL, &textRect);
    SDL_RenderCopy(ctx, sign, NULL, &signRect);
    SDL_RenderFillRect( ctx, &fillRect );
    SDL_RenderPresent( ctx );
}