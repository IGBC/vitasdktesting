#pragma once
#include "AudioEngine.h"
#include "ControllerProxy.h"

#include <SDL2/SDL.h>

class Game {
    constexpr static int SCREEN_WIDTH  = 960;
    constexpr static int SCREEN_HEIGHT = 544;

    int x,y,dx,dy;
    SDL_Texture *text, *sign;

    AudioEngine audio;
    ControllerProxy controls;

public:
    Game(SDL_Renderer* ctx);
    Game(Game&) = delete;
    bool update(float delta);
    void draw(SDL_Renderer* ctx);
};